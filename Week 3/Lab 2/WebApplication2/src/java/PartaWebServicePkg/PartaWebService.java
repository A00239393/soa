/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PartaWebServicePkg;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author A00239393
 */
@WebService(serviceName = "PartaWebService")
public class PartaWebService {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getAllParts")
    public String getAllParts() {
        //TODO write your implementation code here:
        StringBuffer buffer = new StringBuffer();
        
        buffer.append("<parts>\n");
        buffer.append("<partsID>123</partsId>\n");
        buffer.append("<partsID>456</partsId>\n");
        buffer.append("<partsID>789</partsId>\n");
        buffer.append("<parts>\n");
        
        return buffer.toString();
    }
}
